import * as React from "react";
import Head from "next/head";
import {
  Box,
  Button,
  Container,
  Pagination,
  Stack,
  SvgIcon,
  Typography,
  Unstable_Grid2 as Grid,
  Divider,
  IconButton,
} from "@mui/material";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import ListSubheader from "@mui/material/ListSubheader";
import Switch from "@mui/material/Switch";
import PropTypes from "prop-types";
import PencilIcon from "@heroicons/react/24/solid/PencilIcon";
import PencilSquareIcon from "@heroicons/react/24/solid/PencilSquareIcon";
import { toast } from "../../components/toast-container";

function formatDate(date) {
  const options = {
    year: "numeric",
    month: "long",
    day: "numeric",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
    timeZoneName: "short",
  };
  return new Date(date).toLocaleString("en-US", options);
}

export const FeatureFlagList = ({ featureflaglist, env, projectKey }) => {
  const [featureflagStatus, setFeatureFlagStatus] = React.useState(
    featureflaglist.environments.hasOwnProperty(env) && featureflaglist.environments[env].on
  );
  const [lastUpdated, setLastUpdated] = React.useState(
    featureflaglist.environments.hasOwnProperty(env) &&
      featureflaglist.environments[env].lastModified
  );
  const [isSwitchDisabled, setIsSwitchDisabled] = React.useState(true);

  const onSwitchEdit = () => {
    setIsSwitchDisabled(!isSwitchDisabled);
  };
  const handleToggle = (value) => async () => {
    const newChecked = value;
    try {
      const response = await fetch(
        `./api/featureflag?key=${featureflaglist.key}&env=${env}&value=${newChecked}&projectKey=${projectKey}`
      );
      const data = await response.json();
      if (
        data &&
        data.hasOwnProperty("environments") &&
        data.environments.hasOwnProperty(env) &&
        data.environments[env].on === newChecked
      ) {
        toast.success(`${featureflaglist.key} value is updated successfully`);
        setFeatureFlagStatus((prev) => (prev = newChecked));
        setLastUpdated((prev) => (prev = new Date().toISOString()));
      } else {
        toast.error(`${featureflaglist.key} value is not updated.`);
      }
      onSwitchEdit();
    } catch (error) {
      console.error("Error calling server function:", error);
    }
  };
  return (
    <Grid xs={12} md={6} lg={4} key={featureflaglist.id}>
      <List
        sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
        subheader={<ListSubheader>{featureflaglist.name}</ListSubheader>}
      >
        <ListItem>
          <ListItemText
            primary={featureflaglist.key}
            secondary={
              <React.Fragment>
                <Typography
                  sx={{ display: "inline" }}
                  component="span"
                  variant="body2"
                  color="text.primary"
                >
                  {featureflaglist.description}
                </Typography>
              </React.Fragment>
            }
          />
          <Switch
            edge="end"
            disabled={isSwitchDisabled}
            onChange={handleToggle(!featureflagStatus)}
            checked={featureflagStatus}
            inputProps={{
              "aria-labelledby": "switch-list-label-wifi",
            }}
          />
          <IconButton onClick={onSwitchEdit} disableRipple>
            <SvgIcon fontSize="small" sx={{ ml: 1 }}>
              <PencilSquareIcon />
            </SvgIcon>
          </IconButton>
        </ListItem>
        <Divider component="li" />
        <ListItem>
          <Typography
            sx={{ display: "inline" }}
            component="span"
            variant="caption"
            color="text.secondary"
          >
            Last Updated:
          </Typography>
          <Typography
            sx={{ display: "inline" }}
            component="span"
            variant="caption"
            color="text.secondary"
          >
            {formatDate(lastUpdated)}
          </Typography>
        </ListItem>
      </List>
    </Grid>
  );
};

FeatureFlagList.propTypes = {
  featureflaglist: PropTypes.object.isRequired,
};
