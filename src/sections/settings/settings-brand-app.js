import { useCallback, useState } from "react";
import {
  Card,
  CardContent,
  CardHeader,
  Checkbox,
  Divider,
  FormControlLabel,
  FormControl,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
  Typography,
  Unstable_Grid2 as Grid,
  CardActions,
  Button,
} from "@mui/material";
import { useGlobalState } from "../../contexts/global-state-context";
import { useRouter } from 'next/router';

export const SettingsBrandApp = () => {
  const router = useRouter();
  const { state, dispatch } = useGlobalState();
  const [selectedValue, setSelectedValue] = useState(state.projectKey);
  const handleChange = (event) => {
    dispatch({ type: "SET_PROJECT_KEY", payload: event.target.value });
    setSelectedValue(event.target.value);
    router.push(`/?projectKey=${event.target.value}`);
  };
  const handleSubmit = useCallback((event) => {
    event.preventDefault();
  }, []);

  return (
    <form onSubmit={handleSubmit}>
      <Card>
        <CardHeader subheader="" title="Brand.com Apps" />
        <Divider />
        <CardContent>
          <Grid container spacing={6} wrap="wrap">
            <Grid xs={12} sm={6} md={4}>
              <Stack spacing={2}>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Choose an option:</FormLabel>
                  <RadioGroup
                    aria-label="options"
                    name="options"
                    value={selectedValue}
                    onChange={handleChange}
                  >
                    <FormControlLabel value="cb" control={<Radio />} label="CB" />
                    <FormControlLabel value="era" control={<Radio />} label="ERA" />
                    <FormControlLabel value="bhgre" control={<Radio />} label="BHGRE" />
                  </RadioGroup>
                </FormControl>
              </Stack>
            </Grid>
          </Grid>
        </CardContent>
        {/* <Divider />
        <CardActions sx={{ justifyContent: "flex-end" }}>
          <Button variant="contained">Save</Button>
        </CardActions> */}
      </Card>
    </form>
  );
};
