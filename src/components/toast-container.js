// components/ToastContainer.js

import { ToastContainer as OriginalToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const ToastContainer = () => {
  return <OriginalToastContainer />;
};

export { toast, ToastContainer };
