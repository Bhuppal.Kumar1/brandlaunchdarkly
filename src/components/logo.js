import { useTheme } from '@mui/material/styles';
import { useGlobalState } from '../contexts/global-state-context';

export const Logo = (props) => {
  const { state, dispatch } = useGlobalState();
  const { type, app } = props;
  return (
    <>
      {type === 'light1'
        ? app === 'cb' ? 
        <img src="../assets/logos/cb_logo.png" /> : 
        state.projectKey === 'era' ? <img src="../assets/logos/era_logo.svg" style={{
          width: '100px', // Adjust the width as needed
          height: '100px', // Adjust the height as needed
          objectFit: 'cover', // Adjust the object fit property as needed
        }}/> : 
        <img src="../assets/logos/bhgre_logo.svg" style={{
          width: '150px', // Adjust the width as needed
          height: '80px', // Adjust the height as needed
          objectFit: 'cover', // Adjust the object fit property as needed
          borderRadius: '8px', // Optional: Add rounded corners
        }} />
        : <img src="../assets/logos/launchdarkly_logo.png" />
      }
    </>
  );
};
