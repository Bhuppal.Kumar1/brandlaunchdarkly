import React, { createContext, useContext, useReducer } from "react";

// Define your initial state and reducer function
const initialState = {
  counter: 0,
  projectKey: "cb",
};

const reducer = (state, action) => {
  switch (action.type) {
    case "SET_PROJECT_KEY":
      return { ...state, projectKey: action.payload };
    case "INCREMENT":
      return { ...state, counter: state.counter + 1 };
    case "DECREMENT":
      return { ...state, counter: state.counter - 1 };
    default:
      return state;
  }
};

// Create the context
const GlobalStateContext = createContext();

// Create a provider component to wrap your app with
export const GlobalStateProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <GlobalStateContext.Provider value={{ state, dispatch }}>
      {children}
    </GlobalStateContext.Provider>
  );
};

// Create a custom hook to simplify accessing the context
export const useGlobalState = () => {
  return useContext(GlobalStateContext);
};
