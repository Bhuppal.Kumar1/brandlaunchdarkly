export const getAllFeatureFlags = async (env, projectKey) => {

 const apiUrl = `https://app.launchdarkly.com/api/v2/flags/${projectKey}-brand-site`;
 const apiParams = {
   env: env,
   sort: '-creationDate',
 };

 const headers = {
   'Content-Type': 'application/json',
   Authorization: `api-6846e7f6-022e-4a50-a767-f6e7ed5a9514`,
 };

 const urlWithParams = new URL(apiUrl);
 Object.keys(apiParams).forEach((key) => urlWithParams.searchParams.append(key, apiParams[key]));

 const response = await fetch(urlWithParams.toString(), {
    method: 'GET',
    headers: headers,
  });

  if (!response.ok) {
    console.error(`Error fetching data. Status code: ${response.status}`);
    return {
      notFound: true,
    };
  }
  const data = await response.json();
  let resultFeatureFlag = data.items.map((item, index) => {
    return {
        id: index + 1,
        name: item.name,
        description: item.description,
        key: item.key,
        environments: item.environments,
    }
  })
  return {
        resultFeatureFlag,
  };
}


export const updateFeatureFlagByKey = async (key, env, value, projectKey ) => {
  const apiUrl = `https://app.launchdarkly.com/api/v2/flags/${projectKey}-brand-site/${key}`;

 const headers = {
   'Content-Type': 'application/json; domain-model=launchdarkly.semanticpatch',
   Authorization: `api-6846e7f6-022e-4a50-a767-f6e7ed5a9514`,
 };

 const postDataFeatureflagOn = {
    comment: "Turning on flag.",
    environmentKey: env,
    instructions: [
        {
            kind: "turnFlagOn"
        }
    ]
};

const postDataFeatureflagOff = {
  comment: "Turning off flag.",
  environmentKey: env,
  instructions: [
      {
          kind: "turnFlagOff"
      }
  ]
};
const body = JSON.stringify(value === 'true' ? postDataFeatureflagOn : postDataFeatureflagOff);

 const response = await fetch(apiUrl.toString(), {
    method: 'PATCH',
    headers: headers,
    body: body,
  });

  if (!response.ok) {
    console.error(`Error fetching data. Status code: ${response.status}`);
    return {
      notFound: true,
    };
  }
  const data = await response.json();

  return {
    data,
  };
}