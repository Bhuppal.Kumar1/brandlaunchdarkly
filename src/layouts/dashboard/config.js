import CogIcon from "@heroicons/react/24/solid/CogIcon";
import { SvgIcon } from "@mui/material";

export const items = [
  {
    title: "Dev",
    path: "/",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="#ADD8E6"
        viewBox="0 0 24 24"
        stroke="red"
        width="18"
        height="18"
      >
        <rect width="18" height="18" x="3" y="3" rx="2" ry="2" />
      </svg>
    ),
  },
  {
    title: "QA",
    path: "/qa",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="#E6E6FA"
        viewBox="0 0 24 24"
        stroke="red"
        width="18"
        height="18"
      >
        <rect width="18" height="18" x="3" y="3" rx="2" ry="2" />
      </svg>
    ),
  },
  {
    title: "UAT",
    path: "/uat",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="#FFFFE0"
        viewBox="0 0 24 24"
        stroke="red"
        width="18"
        height="18"
      >
        <rect width="18" height="18" x="3" y="3" rx="2" ry="2" />
      </svg>
    ),
  },
  {
    title: "HotFix",
    path: "/hotfix",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="#FFB6C1"
        viewBox="0 0 24 24"
        stroke="red"
        width="18"
        height="18"
      >
        <rect width="18" height="18" x="3" y="3" rx="2" ry="2" />
      </svg>
    ),
  },
  {
    title: "Production",
    path: "/production",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="#90EE90"
        viewBox="0 0 24 24"
        stroke="red"
        width="18"
        height="18"
      >
        <rect width="18" height="18" x="3" y="3" rx="2" ry="2" />
      </svg>
    ),
  },
  {
    title: "Settings",
    path: "/settings",
    icon: (
      <SvgIcon fontSize="small">
        <CogIcon />
      </SvgIcon>
    ),
  },
];
