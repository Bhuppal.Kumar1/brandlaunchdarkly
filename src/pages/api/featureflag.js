/* eslint-disable import/no-anonymous-default-export */
import { updateFeatureFlagByKey } from '../../services/launchdarkly';

export default async (req, res) => {
    const { key, env, value, projectKey } = req.query;
    const { data: responseUpdateFeatureFlag } = await updateFeatureFlagByKey(key, env, value, projectKey );
    return res.status(200).json({ ...responseUpdateFeatureFlag || null });
};