import * as React from "react";
import Head from "next/head";
import {
  Box,
  Container,
  Stack,
  Typography,
  Unstable_Grid2 as Grid,
} from "@mui/material";
import { Layout as DashboardLayout } from "src/layouts/dashboard/layout";
import { FeatureFlagList } from "src/sections/featureflag/featureflag-list";
import { getAllFeatureFlags } from "src/services/launchdarkly";
import { useGlobalState } from '../contexts/global-state-context';

const Page = ({ resultFeatureFlag }) => {
  const { state, dispatch } = useGlobalState();
  return (
    <>
      <Head>
        <title>Brand | QA</title>
      </Head>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 8,
        }}
      >
        <Container maxWidth="xl"
style={{ backgroundColor: "#E6E6FA" }}>
          <Stack spacing={2}>
            <Stack direction="row"
justifyContent="space-between"
spacing={4}>
             <Stack spacing={1}
direction="row"
alignItems="center"
sx={{ color: "darkblue", mt:2 }}>
             <Typography variant="h5"
sx={{ color: "grey" }}>Environment:</Typography>
                <Typography variant="h5" >QA</Typography>
              </Stack>
              <Stack alignItems="center"
direction="row"
spacing={1}>
                  <Typography style={{ color: "grey" }}
variant="subtitle1">Count:</Typography>
                  <Typography variant="subtitle1" >{resultFeatureFlag.length}</Typography>
                </Stack>
            </Stack>
            <Grid container
spacing={3}>
              {
                resultFeatureFlag.map((item, index) => {
                  return (
                    <FeatureFlagList featureflaglist={item}
key={index}
env='qa'
projectKey={state.projectKey}/>
                  )
                })
              }
            </Grid>
            <Box
              sx={{
                display: "flex",
                justifyContent: "center",
              }}
            ></Box>
          </Stack>
        </Container>
      </Box>
    </>
  );
};

export async function getServerSideProps(context) {
  const { query } = context;
  const projectKey = query.projectKey || 'cb';
  const data = await getAllFeatureFlags('qa',projectKey);
  const { resultFeatureFlag } = data;
 
  return {
    props: {
      resultFeatureFlag,
    },
  };
}

Page.getLayout = (page) => <DashboardLayout>{page}</DashboardLayout>;

export default Page;
