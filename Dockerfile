FROM node:16-alpine
RUN mkdir -p /app
WORKDIR /app
COPY . .
RUN npm install 
RUN npm run build
ENV PORT=80

ENV NODE_ENV dev

EXPOSE 80
CMD ["npm", "start"]
